// Meteor.startup(function() {
//   var CronHistory = Meteor.isClient ? window : global;
//   CronHistory = SyncedCron._collection;
//   CronHistory.attachSchema(new SimpleSchema({
//     name: {
//     type: String,
//     label: "Name"
//     }
//   }));
// });

// AdminConfig = {
//   adminEmails: ['theabstractarts@gmail.com'],
//   collections: {
//     Events: {
//       tableColumns: [
//         {label: 'Name', name: 'name'},
//         {label: 'Date', name: 'date'},
//         {label: 'Host', name: 'host'}
//       ]
//     },
//     ConfigValues: {
//       tableColumns: [
//         {label: 'Name', name: 'name'},
//         {label: 'Value', name: 'value'}
//       ]
//     }
//     // CronHistory: {
//     //   tableColumns: [
//     //     {label: 'Name', name: 'name'}
//     //   ]
//     // }
//   }
// };


ConfigValues = new Mongo.Collection("configValues");

ConfigValues.attachSchema(new SimpleSchema({
  name: {
    type: String,
    label: "Name",
    max: 200
  },
  value: {
    type: String,
    label: "Value",
    max: 200
  }
}));



// Houston.add_collection(Meteor.users);
// Houston.add_collection(ConfigValues);
// Houston.add_collection(Houston._admins);