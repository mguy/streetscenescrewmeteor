Router.route('/', function() {
	this.render('home');
});

Router.route('/user/:_username', {
  name: 'user_profile',
  data: function() { return Meteor.user();} 
});

Router.route('/create', function() {
  this.render('createEvent');
}, {
  name: 'create'
});

Router.onBeforeAction(function () {
  if (!Meteor.userId()) {
    // if the user is not logged in, render the Login template
    Router.go('/sign-in');
  } else {
    // otherwise don't hold up the rest of hooks or our route/action function
    // from running
    this.next();
  }
}, {
  only: ['create']
});

Router.route('/insert', function() {
  this.render('insertEvent');
});

Router.route('/events/:slug', {
  name: 'event',
  data: function() { return Events.findOne({slug: this.params.slug});}
});

Router.route('/editevents/:slug', {
  name: 'eventEdit',
  data: function() { return Events.findOne({slug: this.params.slug});}
});


Router.route('/useremail', function() {
	this.render('userEmail');
}, {
	name: 'userEmail'
});


Router.route('/testbutton', function() {
  this.render('testButton');
}, {
  name: 'testButton',
  waitOn: function () {
    return Meteor.subscribe('cronHistory');
  }
});



Router._filters = {
  hasCompletedProfile: function() {
    if(!this.ready()) return;
    var user = Meteor.user();
    if (user && ! userProfileComplete(user)){
      this.render('userEmail');
    } else {
      this.next();
    } 
  }, 
}; 

// copied from Telesc.pe router.js
filters = Router._filters;
Router.onBeforeAction(filters.hasCompletedProfile);


Router.configure({
  layoutTemplate: 'layout',
});