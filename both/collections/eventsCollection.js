Events = new Mongo.Collection("events");

Events.friendlySlugs('name');

// Tomorrow set for min date validation 
var Tomorrow = moment().add(1, 'days').toDate();

SimpleSchema.messages({
  "endTimeTooSoon": "End time needs to be after start time",
  "endTimeTooLate": "Advertised end time should probably not be more than \
  4 hours after the start. Stay later if you like, but you should be sure \
  you can stay as long as this end time."
});

Events.attachSchema(new SimpleSchema({
  name: {
    type: String,
    label: "Event Name",
    max: 200
  },
  host: {
    type: String,
    label: "Host",
    optional: true,
    autoValue: function() {
      return this.userId;
    },
  },
  hostName: {
    type: String,
    label: "Host Name",
    autoValue: function() {
      if(this.isInsert) {
          try {
          var normalName = Meteor.user().profile.name;
            return normalName;
          } catch(err) {
            console.log(err);
          };
          console.log('normalName'+'normalName');
          console.log(normalName);
          try {
            var fbName = Meteor.user().services.facebook;
            return fbName;
          } catch(err) {
            console.log(err);
          };      
          console.log('CHECK EM');
          console.log('Hostname'+'fbName');   
          console.log(fbName);
          if (normalName) {
            var name = normalName
          } else {
            var name = fbName
          };
          return name;
      };
    },
  },
  crew: {
    type: [String],
    label: "Crew",
    // Set host user as first crew member on event creation.
    // Further opertions on the collection are not insert methods
    // so this function is not invoked.
    autoValue: function() {
      if (this.isInsert) {
      return [
        this.userId
        ];
      };
    },
    // defaultValue is the non-dynamic version of autoValue.
    // This would create an empty array.
    // defaultValue: []
  },
  crewNames: {
    type: [String],
    label: "Crew names",
    optional: true
  },
  location: {
    type: String,
    label: "Location (in detail – this is how people will find you)"
  },
  date: {
    type: Date,
    label: "Date",
    min: Tomorrow,
    autoform: {
      afFieldInput: {
        type: "bootstrap-datetimepicker",
        autoclose: "true"
      }
    }
  },
  eventlength: {
    type: 'select-multiple',
    label: "How long will you be there for? (Give a minimum time and \
      make sure to stay so people don't come and find nobody!)",
    min: 1,
    defaultValue: '2 hours'
  },
// TODO remove endtime below and replace with eventlength variable for simpler UX -
// This requires updating ../tests/events/events-test.js before removing endtime entirely
  endtime: {
    type: Date,
    label: "End time (so people don't come after you leave)",
    optional: true,
    // custom: function () {
    //   if (this.value < this.field('date').value ) {
    //     return "endTimeTooSoon";
    //   } else if (this.value.getTime() > (this.field('date').value.getTime() + (60*60*1000*4) ) ) {
    //     return "endTimeTooLate";
    //   }
    // },
    autoform: {
      afFieldInput: {
        type: "bootstrap-datetimepicker",
        autoclose: "true"
      }
    }
  },
  eventdescription: {
    type: String,
    label: "Describe your event",
    max: 1000
  },
  confirmedAt: {
    type: Date,
    optional: true
  },
  createdAt: {
    type: Date,
    autoValue: function() {
      if (this.isInsert) {
        return new Date();
      } else {
        this.unset();  // Prevent user from supplying their own value
      };
    }
  },
  facebookId: {
    type: String,
    optional: true
  }
}));

if (Meteor.isClient) {
  Meteor.subscribe('events');
  Meteor.subscribe('config');
};