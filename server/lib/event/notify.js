// Notes and Methods to send FB page post updates, etc.
// Graph API disallows event create, so we notify people of events 
// with feed updates
// Graph explorer https://developers.facebook.com/tools/explorer/

// TODO Check FutureNotifications carries over working notifications
// on server restart 

// https://github.com/percolatestudio/meteor-synced-cron
// because original doesn't allow one-off events orphan:synced-cron

// https://richsilv.github.io/meteor/scheduling-events-in-the-future-with-meteor/

// curl --data "message="Less%20Go"&access_token=CAADzZAhkaGX0BABWXS0SDVvu7upA1CRl\
//IXfOUuANuMDvBZA8F7nu3nzZCD0dRcvhvKNUIqLH4sw1r6wjYLLPqy0WoRo8VvPVpQQFcZBGvVwE2VsT\
//iS913SlPMFIQ6xm3pHROCcvudFN5NG6LyipwfoIibgpqDCOR1z1NkxlEwZAgZBwpz60dMCQ2Kg1EMwm4AZD"
//https://graph.facebook.com/v2.3/426364720649/feed
// {"id":"426364720649_10155713569775650"}%

var comingNotifications = JobCollection('comingNotifications');

comingNotifications.startJobServer();

var workers = comingNotifications.processJobs('facebookPagePost',
	  {
	    concurrency: 4,
	    payload: 1,
	    pollInterval: 10
	  },	
      function (job, cb) {
      	console.log('worker working');
		var event = job.data.event;
		var NotifyId = job.data.notifyId;
		console.log(job.data);
		console.log(NotifyId);
		facebookPageNotify(event, NotifyId);
		job.done();
		cb();
      }
    );

// Collection storing tasks in case of server restart

// Notify functions refactorisation
// 1 function takes array of notification days
// 2 for each loop sets up synced cron

// New refactored notification function

// Each of the 'eventX' parameters is an integer representing a notification
// and the number of days before the event that the notification needs to be made

var addNotification = function(id, event, event1, event2, event3) {
console.log('Full notification service (i.e. not test notification)');

var notificationDays = [event1, event2, event3];

	var notificationDate = new Date(event.date);
	notificationDate.setHours(15);
	notificationDate.setMinutes(29);
	
	notificationDays.forEach(function(daysBefore, index) {
		var loopNotificationDate = notificationDate;
		loopNotificationDate.setDate(loopNotificationDate.getDate() - daysBefore);
		console.log(loopNotificationDate);
		if ( loopNotificationDate > new Date() ) {
			var indexPlus = index + 1;	
			console.log('EVENT NOTIFICATION ' + indexPlus + ' ADDED - NON-STAGING ');

			console.log(event.name);
			console.log(event.date);
			console.log('Notification datetime:');
			console.log(loopNotificationDate);
			console.log('Current datetime:');
			var dateNow = new Date();
			console.log(dateNow);
		
			var idWithIndex = event.name + 'IIII' + id + 'Notification' + indexPlus;

			console.log('All vars for syncedCron add');
			console.log('––loopNotificationDate');
			var fullDate = loopNotificationDate;
			console.log(fullDate);
			console.log('––Event');
			console.log(event);
			console.log('––idWithIndex');
			console.log(idWithIndex);

			var newNotify = new Job(comingNotifications,
									'facebookPagePost',
									{
										notifyId: idWithIndex,
										event: event,
									}
									);
			newNotify.after(loopNotificationDate)
			.priority('normal').retry({ retries: 5,
	        wait: 150 })
	      	.save();



			// SyncedCron.add({
			// 	name: idWithIndex,
			// 	schedule: function(parser) {
			// 		return parser.recur().on(loopNotificationDate).fullDate();
			// 	},
			// 	job: function() {
			// 		var NotifyId = idWithIndex;
			// 		facebookPageNotify(event, NotifyId);
			// 		FutureNotifications.remove(idWithIndex);
			// 		SyncedCron.remove(idWithIndex);
			// 		return "Notification removed: " + idWithIndex;
			// 	}
			// });
		};
	}); 
};


// TODO 3 test notifications, to be posted just after event creation for test purposess,
// which are run over 3 consequent minutes beginning
// at notificationDate.setHours(x); and notificationDate.setMinutes(x);
// on the date 1 day before the dg pate of the test event (which has been created for test).

var addTestNotification = function(id, event, event1, event2, event3) {
console.log('Test notification service');

var notificationSeconds = [event1, event2, event3];
	
var testNotificationDate = new Date();
testNotificationDate.setSeconds(testNotificationDate.getSeconds() + 55);
notificationSeconds.forEach(function(secondsBefore, index) {
	testNotificationDate.setSeconds(testNotificationDate.getSeconds() - secondsBefore);
	if ( testNotificationDate > new Date() ) {
	var indexPlus = index + 1;	
	console.log('EVENT NOTIFICATION ' + indexPlus + ' ADDED - Test ' +
		secondsBefore + ' seconds before');
	console.log(event.name);
	console.log(event.date);
	console.log(testNotificationDate);
		var idWithIndex = event.name + 'IIII' + id + 'notification' + indexPlus;

		console.log('All vars for syncedCron add');
		console.log('––loopNotificationDate');
		var fullDate = testNotificationDate;
		console.log(fullDate);
		console.log('––Event');
		console.log(event)
		console.log('––idWithIndex');
		console.log(idWithIndex);

		var newNotify = new Job(comingNotifications,
								'facebookPagePost',
								{
									notifyId: idWithIndex,
									event: event,
								}
								);
		newNotify.after(testNotificationDate)
		.log('### JobCollection job logged ###')
		.priority('normal').retry({ retries: 5,
        wait: 150 })
      	.save();

		// SyncedCron.add({
		// 	name: idWithIndex,
		// 	schedule: function(parser) {
		// 		return parser.recur().on(testNotificationDate).fullDate();
		// 	},
		// 	job: function() {
		// 		var NotifyId = idWithIndex;
		// 		facebookPageNotify(event, NotifyId);
		// 		FutureNotifications.remove(idWithIndex);
		// 		SyncedCron.remove(idWithIndex);
		// 		return "Notification removed: " + idWithIndex;
		// 	}
		// });
	};
}); 
};


// Schedule function: called on event confirm
Meteor.methods({
	'scheduleNotification': function(event) {
		var thisId = event._id;
		console.log('Event Scheduled');
		if (ConfigValues.find({name: 'isStaging'}).fetch()[0].value == 'No') { 
			return addNotification(thisId, event, 1, 2, 3)
		} else if (ConfigValues.find({name: 'isStaging'}).fetch()[0].value == 'Yes') {
			// 
			return addTestNotification(thisId, event, 2, 1, 3)
		};
		console.log('Event Scheduled');
		return true;
	}
});

// Function takes confirmed event name, description, date, location
// posts to Facebook page as per https://developers.facebook.com/docs/graph-api/reference/v2.4/page/feed#publish

var facebookPageNotify = function(event, NotifyId) {
	var facebook_access_token = process.env.FACEBOOK_ACCESS_TOKEN;
	console.log(facebook_access_token);

	var facebook_page_id = process.env.FACEBOOK_PAGE_ID;
	console.log(facebook_page_id);

	FBGraph.setAccessToken(facebook_access_token);

	var eventDateFormatted = moment(event.date).
		format('h:mma dddd Do MMM YYYY');

	console.log('FACEBOOK NOTIFY id');
	console.log('Notification date time:' + Date());
	console.log(NotifyId);
	console.log('Event date');
	console.log(event.date);
	console.log('Event date formatted');
	console.log(eventDateFormatted);

	// 
	if (NotifyId.match(/notification3/i)) {
		eventOpener = "Event announced for ";
		linkImage = "https://s3-eu-west-1.amazonaws.com/www.streetscenes.org.uk/crew-images/street-scenes-smellery.jpg"
	} else if (NotifyId.match(/notification2/i)) {
		eventOpener = "Coming soon, a street gathering at ";
		linkImage = "https://s3-eu-west-1.amazonaws.com/www.streetscenes.org.uk/crew-images/street-scenes-letter-writing.jpg"
	} else {
		eventOpener = "Last reminder for event at ";
		linkImage = "https://s3-eu-west-1.amazonaws.com/www.streetscenes.org.uk/crew-images/street-scenes-patches.jpg"
	}

	var message_build = eventOpener + eventDateFormatted + "\n"
		+ "Location: " + event.location
		+ "An event called " + event.name + "\n"
		+ event.eventdescription + "\n";
	var message = { 
		message: message_build,
		link: linkImage
	};
	FBGraph.post(facebook_page_id + "/feed", message, function(err, res) {
		console.log(res);
	});
};

// TODO Remove this: Test method which makes the above Facebook function available on client
Meteor.methods({
	'facebookPageNotifyGlobal': function(event, NotifyId) {
		facebookPageNotify(event, NotifyId);
	}
});

// Startup function which resets cron from FutureNotifications collection
// Meteor.startup(function() {
// 	    var SyncedCronLogger = function(opts) {
//       		// console.log('Level', opts.level);
// 	      	// console.log('Message', opts.message);
//       		// console.log('Tag', opts.tag);
//     	}
// 	    SyncedCron.config({
// 	        logger: SyncedCronLogger
// 	    });
//     FutureNotifications.find().forEach(function(event) {
//         if (event.date > new Date()) {
//         	console.log('FutureNotifications setting at startup');
//         	console.log(event.name);
//         	console.log(event._id);
//             addNotification(event._id, event, 1, 3, 3);
//         }
//     });
//     SyncedCron.start();
//  });