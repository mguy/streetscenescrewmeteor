/*
This contains mailing methods including methods to extract email lists from complex profile lists.
*/

// gather all user emails to send out crew email
function allUserEmails() {
	var allEmails = Meteor.users.find({}).map(function(user){
	// As we have two kinds of plugin-created user records, we have
	// two kinds of email. Here we summon each in different ways, using
	// a try-catch pattern on each, because they throw fatal errors 
	   try {
	   	if ((typeof user.profile.email) == 'string') {
		   	return user.profile.email;
		   	console.log(user.profile.email);
		   }
	   }
	   catch(err) {
	   	console.log('Profile error caught and dealt with - mailFunctions.js');
	   	console.log(err.message);
	   }
	   try {
	   	if (user.emails[0].address) {
			return user.emails[0].address;
		} 
	   }
	   catch(err) {
		console.log('Profile error caught and dealt with - mailFunctions.js');
	   	console.log(err.message);
	   }
	});
	// TODO - The 3 lines below were introduced to remove the event host's email from this list of users emails e.g. when sending requests for an event. They were returning one random email address. This is a possible todo as the functionality maybe ok as it is.
	// var currentUser = Meteor.user();
	// var index = allEmails.indexOf(currentUser.profile.email);
	// allEmails.splice(index, 1);
	return allEmails;
};

// gather all emails of a particular event's crew to send out crew reminder
function allHostEmails(hosts) {
	var allEmails = hosts.map(function(user){
	// As we have two kinds of plugin-created user records, we have
	// two kinds of email. Here we summon each in different ways, using
	// a try-catch pattern on each, because they throw fatal errors
		var user = Meteor.users.find(user).fetch()[0];
	   try {
	   	if ((typeof user.profile.email) == 'string') {
		   	var done = 'True';
		   	return user.profile.email;
		   }
	   }
	   catch(err) {
	   	console.log('No profile error caught and dealt with - mailnewevent.js');
	   	console.log(err.message);
	   }
	   	try {
	   	if (user.emails[0].address) {
			return user.emails[0].address;
		} 
	   }
	   catch(err) {
		console.log('No profile error caught and dealt with - mailnewevent.js');
	   	console.log(err.message);
	   }
	})
	return allEmails;
};

// TODO Following two methods can/should be refactored

Meteor.methods({
	emailUsers: function(options) {
		// check(text, String);
		// this.unblock();
		// options object needs allUserEmails added as key-value pair 
		if (ConfigValues.find({name: 'emailsActive'}).fetch()[0].value == 'Yes') { 
			options['from'] = "Street Scenes <info@streetscenes.org.uk>";
			if(options['crew']) {
				var crew = options['crew'];
				var bcc = allHostEmails(crew);
			} else {
				var bcc = allUserEmails();
			}
			options['bcc'] = bcc;
			console.log('Email event info to all selected users');
			console.log(Date());
			console.log(options);
			Email.send(options);
		};
	}
});

// Remind hosts of event day before.
// This function can be refactored into a general event update email - 
// as in client/views/events/event.js
Meteor.methods({
		remindEventEmail: function(id, hosts, url, rootUrl, date, description, name, location) {
		if (ConfigValues.find({name: 'emailsActive'}).fetch()[0].value == 'Yes') { 
			console.log(name);
			// dateString = date.toString();
			var dataContext = {
				message: "You have confirmed your participation in a Street Scenes event tomorrow. Please be sure to come and support your team",
				url: url,
				rootUrl: rootUrl,
				date: date,
				// date: dateString,
				name: name,
				location: location,
				description: description,
			};
			console.log('dataContext');
			console.log(dataContext);

			// TODO remove my email from this

			Mailer.send({
				to: 'theabstractarts@gmail.com',
				from: 'Street Scenes <info@streetscenes.org.uk>',
				subject: 'Your Street Scenes event is tomorrow',
				template: 'remindEventEmail',
				data: {
					url: url,
					rootUrl: rootUrl,
					date: date,
					description: description,
					name: name,
					location: location
				}
		});

		// var html = Blaze.toHTMLWithData(Template.remindEventEmail, dataContext);

		console.log('html');
		console.log(html);

		var options = {
				bcc: hosts.email,
				subject: 'Your Street Scenes event is tomorrow',
				// text: 'hits'
				html: html
		};

		// Meteor.call('emailToHosts', options);
	};
	}
});

Meteor.methods({
	emailToHosts: function(options) {
		if (ConfigValues.find({name: 'emailsActive'}).fetch()[0].value == 'Yes') { 
		// check(text, String);
		// this.unblock();

		// options object needs allUserEmails added as key-value pair
		var hosts = options['hosts'];
		console.log('email to hosts called');
		var hostEmails = allHostEmails(hosts);
		options['from'] = "Street Scenes <info@streetscenes.org.uk>";
		options['bcc'] = hostEmails;
		console.log(hostsEmails);
		console.log(options);
		Email.send(options);
		};
	}
});

var shell = function () {
    var Future = Npm.require('fibers/future'),
        future = new Future(),
        db = MongoInternals.defaultRemoteCollectionDriver().mongo.db;

    db.collectionNames( 
        function(error, results) {
            if (error) throw new Meteor.Error(500, "failed");
            future.return(results);
        }
    );
    return future.wait();
};