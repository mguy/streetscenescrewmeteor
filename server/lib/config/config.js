 Meteor.startup(function() {


    // Remove configuration entries in case service is already configured
    ServiceConfiguration.configurations.remove({$or: [
        {service: "facebook"},
        // {service: "twitter"}, 
		{service: "google"},
    ]});

    // Add Google configuration entry
    ServiceConfiguration.configurations.insert({
        "service": "google",
        "clientId": "451978170547-8vvvp3dq0b3l5rl7ah3mpb1rput27432.apps.googleusercontent.com",
        "secret": "1XDTf_ChC2QqNZmQAF94ayO3"
    });

    var fb_appId = process.env.FACEBOOK_APPID;
    var fb_secret = process.env.FACEBOOK_SECRET;

    // Add Facebook configuration entry
    ServiceConfiguration.configurations.insert({
        "service": "facebook",
        "appId": fb_appId, 
        "secret": fb_secret 
    });

    Accounts.emailTemplates.siteName = "Street Scenes";
    Accounts.emailTemplates.from = "Street Scenes <info@streetscenes.org.uk>";
    Accounts.emailTemplates.resetPassword.subject = function(user) {
        return "Change Street Scenes password";
    };
    Accounts.emailTemplates.enrollAccount.subject = function(user) {
        return "Thanks for joining Street Scenes";
    };
    Accounts.emailTemplates.verifyEmail.subject = function(user) {
        return "Thanks for joining Street Scenes";
    };

    // Add Twitter configuration entry
    // ServiceConfiguration.configurations.insert({
    //     "service": "twitter",
    //     "consumerKey": process.env.twitter_consumer_key,//"2JbioorlxnG47YMdPjFNwgdSl",
    //     "secret": process.env.twitter_consumer_secret //"gA8BaF2NfaDCdhxsa4diHFGVWb3wANQMrFwMqtM8hIdGlVkfPh"
    // });

    // Removed sensitive info to settings file
    // process.env.MAIL_URL = 'smtp://EMAIL_ADDRESS:PASSWORD@smtp.gmail.com:587';
     var mailUrl = Meteor.settings.mailUrl;
     process.env.MAIL_URL = mailUrl;


this.Templates = {}
Templates.remindEventEmail = {
    path: 'remindEventEmail.html'
};

Mailer.init({
    templates: Templates
    });

});


ConfigValues = new Mongo.Collection("configs");

ConfigValues.attachSchema(new SimpleSchema({
  name: {
    type: String,
    label: 'Name'
  },
  value: {
    type: String,
    label: 'value'
  }
}));

// Set configValues if they are not already set. Change manually if necessary
Meteor.startup(function() {
    var cCN = ConfigValues.find({name: 'criticalCrewNumber'}).fetch();
    if( typeof cCN[0] == 'undefined' ) {
    ConfigValues.insert(
        {
            name: 'criticalCrewNumber',
            value: '2'
        });
    };
    var eA = ConfigValues.find({name: 'emailsActive'}).fetch();
    if( typeof eA[0] == 'undefined' ) {
    ConfigValues.insert(
        {
            name: 'emailsActive',
            value: 'Yes'
        });
    };
    var iS = ConfigValues.find({name: 'isStaging'}).fetch();
    if( typeof iS[0] == 'undefined' ) {
    ConfigValues.insert(
        {
            name: 'isStaging',
            value: 'Yes'
        });
    };

    // Set http headers to allow external site access to rss page
    WebApp.rawConnectHandlers.use(function(req, res, next) {
      res.setHeader("Access-Control-Allow-Origin", "*");
      return next();
    });

});


 if (Meteor.isServer) {
    Meteor.methods({
        isStaging: function(){
            var staging = process.env.STAGING;
            return staging;
        }
    });
};



