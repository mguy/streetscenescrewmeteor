// https://themeteorchef.com/snippets/adding-an-rss-feed/

RssFeed.publish( 'events', function() {
  var feed = this;


  var date_now = new Date();
  var comingEvents = Events.find( { 
	  date:{ $gt: date_now}
  } );
  var events = []

  comingEvents.forEach(function(event) {
		var crewLength = event.crew.length;
		var criticalCrewNumber = ConfigValues.find({name: 'criticalCrewNumber'}).fetch()[0].value;
		if (crewLength >= criticalCrewNumber) {
			events.push(event);
		}
  });



  var lastEventConfirmed = Events.findOne({}, {sort: {confirmedAt: -1}});
  var lastEventConfirmed = lastEventConfirmed.confirmedAt;

  feed.setValue('title', feed.cdata('Street Scenes Events'));
  feed.setValue('description', feed.cdata( 'Feed of coming Street Scenes events.' ) );
  feed.setValue('link', 'http://streetscenes.org.uk' );
  feed.setValue('pubDate', lastEventConfirmed);
  feed.setValue('lastBuildDate', lastEventConfirmed);
  feed.setValue('ttl', 1);




	events.forEach( function( event ){
		feed.addItem({
			title: event.name,
			description: 'Event description: ' 
			+ event.eventdescription + ' Location: ' + event.location,
			pubDate: event.date.toDateString(),
			guid: 'http://www.streetscenes.org.uk/' + event.date.toDateString().replace(/ /g,''),
			// link: 'http://www.streetscenes.org.uk',
		});
	});

});