Meteor.publish('events', function () {
  		return Events.find();
	});

Meteor.publish('config', function () {
  		return ConfigValues.find();
	});

// TODO remove all SyncedCron remnants

Meteor.publish('cronHistory', function() {;
    return SyncedCron._collection.find();
});