
Template.registerHelper('username', function() {
		try {
			username = Meteor.user().profile.name;
			return username
			} catch(e) {
			};
	});


Template.home.helpers({
	events: function() {
		var date_now = new Date();
		var exist = Events.find({
			// TODO trying to fetch future events only for home listing
			date:{ $gt: date_now}
			}).fetch().length;
		if (exist > 0) {
			return Events.find({date:{ $gt: date_now}});
		} else {
			return false
		};
	}
});

// if (Meteor.isClient) {
// 	Meteor.call('isStaging',  function(err, results) {
// 		var staging = results;
// 		return staging;
// 	});
// };

// Template.home.helpers({
// 	staging: function() {
// 		if (staging) {
// 			console.log('APPID setting present!!');
// 			return  "(Staging site)";
// 		} else {
// 			return false
// 		};
// 	}
// });

Template.home.helpers({
    staging: function () {
        return Template.instance().myAsyncValue.get();
    }
});

Template.home.created = function (){
    var self = this;
    self.myAsyncValue = new ReactiveVar(".");
    Meteor.call('isStaging', function (err, asyncValue) {
        if (err)
            console.log(err);
        else 
            self.myAsyncValue.set(asyncValue);
    });
}


Template.home.helpers({
	isConfirmed: function () {
		var crewNumber = Events.find(this).fetch()[0].crew.length;
		var criticalCrewNumber = ConfigValues.find({name: 'criticalCrewNumber'}).fetch()[0].value;
		if (crewNumber >= criticalCrewNumber) {
			return 'True';
		}
	}
  });

UI.registerHelper('formatTime', function(context, options) {
  if(context)
    return moment(context).format('ddd DD MMM, YYYY, hh:mma');
});
