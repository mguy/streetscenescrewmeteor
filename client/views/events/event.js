// criticalCrewNumber is the number of people required before event is confirmed.
// Update: now set on a variable-by-variable basis from ConfigValues

Template.event.helpers({
	isConfirmed: function () {
		var crewNumber = Events.find(this).fetch()[0].crew.length;
		var criticalCrewNumber = ConfigValues.find({name: 'criticalCrewNumber'}).fetch()[0].value;
		if (crewNumber >= criticalCrewNumber) {
			return 'True';
		};
	}
  });


Template.event.helpers({
	hostName: function () {
		var host = Events.find(this).fetch()[0].hostName;
		return host;
	}
});

Template.event.helpers({
	crewNames: function () {
		var crew = Events.find(this).fetch()[0].crewNames;
		// var names = crew.map(
		// 		function(member) {
		// 			try {
		// 				var name = Meteor.users.find(member).fetch()[0].profile.name;
		// 				return name;
		// 			} catch(err) {
		// 				console.log(err);
		// 			};
		// 			console.log('name');
		// 			console.log(name);
					
		// 			try {
		// 				var fbName = Meteor.users.find(member).fetch()[0].services.facebook;
		// 				return fbName;
		// 			} catch(err) {
		// 				console.log(err);
		// 			};
		// 			console.log('fbName');
		// 			console.log(fbName);
					
		// 			if (name) {
		// 				var name = name
		// 			} else {
		// 				var name = fbName
		// 			};
		// 			console.log('lastname');
		// 			console.log(name);
		// 			return name;
		// 		}
		// );
		function capitalizeFirstLetter(string) {
 		   return string.charAt(0).toUpperCase() + string.slice(1);
		};
		crew = crew.map(function(x){ return capitalizeFirstLetter(x) });
		crew = crew.join(', ');
		return crew;
	}
});

Template.event.helpers({
	user_not_already_joined: function() {
		var userId = Meteor.userId();
		var eventCrewUserIds = Events.find(this).fetch()[0].crew;
		var userInvolved = $.inArray(userId, eventCrewUserIds);
		if ( userInvolved < 0 ) {
			return 'True';
		}
	}
});

Template.event.helpers({
	usersUntilCriticalMass: function() {
		var eventCrewLength = Events.find(this).fetch()[0].crew.length;
		var criticalCrewNumber = ConfigValues.find({name: 'criticalCrewNumber'}).fetch()[0].value;
		var usersUntilCritical = criticalCrewNumber - eventCrewLength;
		return usersUntilCritical;
	}
});


Template.event.events({
    'click #joinCrew': function (e) {
      e.preventDefault();
	  var thisEventId = this._id;
      var self = this;
      sweetAlert({
        title: "Are you sure?",
        text: "Be sure you can arrive on time to support\
        the event creator. Your commitment counts\
        towards this event being confirmed and publicised.",
        type: "info",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Yes, I will help run this event!",
        closeOnConfirm: true
      }, function(isConfirm) {
        if (isConfirm) {
        	var thisUserId = Meteor.userId();
			Events.update(thisEventId, {
	      	$addToSet: {crew: Meteor.userId()}
		      });
			Events.update(thisEventId, {
	      	$addToSet: {crewNames: Meteor.users.find(thisUserId).fetch()[0].profile.name}
		      });
		   //    var currentDocId = this._id;
		   //    console.log(currentDocId);
			  // var currentDoc = Events.findOne(currentDocId);
			  // console.log(currentDoc);
			  // var eventDate = currentDoc.date.toUTCString();      
			  // console.log(eventDate);
			  console.log('Event ID');
			  console.log(thisEventId);
		      if (checkEventParticipantsCriticalMass(thisEventId)) {
		      	var currentDocId = thisEventId;
				var currentDoc = Events.findOne(currentDocId);


				Meteor.call('addEventConfirmedAtTime', currentDocId);

				Meteor.call('scheduleNotification', currentDoc);

				var url = currentDoc.slug;
				var rootUrl = Meteor.absoluteUrl();
				var date = currentDoc.date.toString();
				var description = currentDoc.description;
				var name = currentDoc.name;
				var location = currentDoc.location;
				var crew = Events.find(currentDocId).fetch()[0].crew;
				console.log(name);
		      	confirmEventEmail(url, rootUrl, date, description, name, location, crew);
		      };
          /* Submit form: */
          swal("Event proposed",
          	"Please put this in your diary and keep an eye \
        	on your emails.", "success");
        }
      });
	}
});


// Function to check if event has sufficient confirmed participants
var checkEventParticipantsCriticalMass = function (eventId) {
		var crewNumber = Events.find(eventId).fetch()[0].crew.length;
		console.log(crewNumber);
		criticalCrewNumber = ConfigValues.find({name: 'criticalCrewNumber'}).fetch()[0].value;
		console.log(criticalCrewNumber);
		if (crewNumber == criticalCrewNumber) {
			return 'True';
		}
};

// Confirm event when sufficient crew members.
// This function can be refactored into a general event update email.
var confirmEventEmail = function(url, rootUrl, date, description, name, location, crew) {
	var dataContext = {
		message: "Your forthcoming Street Scenes event has been confirmed.",
		url: url,
		rootUrl: rootUrl,
		date: date,
		name: name,
		location: location,
		description: description,
	};
	console.log('Make template');
	var html = Blaze.toHTMLWithData(Template.newEventEmail, dataContext);
	// console.log(html);

	var options = {
			from: 'info@streetscenes.org.uk',
			subject: 'Street Scenes confirmed event',
			crew: crew,
			html: html
	};

	Meteor.call('emailUsers', options);
};


// On event create form success, go to event
AutoForm.hooks({
  add: {
    onSuccess: function(doc) {
      Router.go('event',{_id: this.docId});
    }
  }
});

// Test helper function used to check that event gets confirmed and notifications
// scheduled in the absence of a click from a user
// Template.event.helpers({
// 	schedule: function () {
//       if (checkEventParticipantsCriticalMass(this._id)) {
//       	var currentDocId = this._id;
// 		var currentDoc = Events.findOne(currentDocId);

// 		console.log('check critical mass');
// 		console.log(currentDoc);
// 		Meteor.call('scheduleNotification', currentDoc);

// 		var url = currentDoc.slug;
// 		var rootUrl = Meteor.absoluteUrl();
// 		var date = currentDoc.date.toUTCString();
// 		var description = currentDoc.description;
// 		var name = currentDoc.name;
// 		var location = currentDoc.location;
// 		console.log(name);
//       	confirmEventEmail(url, rootUrl, date, description, name, location);
//       	return 'True';
//       };
// 	}
//   });
