AutoForm.hooks({
	createEventForm: {
		  before: {
		    insert: function(doc) {
		      var self = this;
		      sweetAlert({
		        title: "Are you sure?",
		        text: "Please make sure the details are\
		        	correct. If you agree, we'll email \
		        	all members of the network announcing the event.",
		        type: "info",
		        showCancelButton: true,
		        confirmButtonColor: "#DD6B55",
		        confirmButtonText: "Yes, I can host this event!",
		        closeOnConfirm: true
		      }, function(isConfirm) {
		        if (isConfirm) {
		          /* Submit form: */
		          self.result(doc);
		          swal("Event proposed",
		          	"Please put this in your diary and keep an eye \
		        	on your emails.", "success");
		        } else {
		          /* Async cancel form submission: */
		          self.result(false);
		        }
		      });
		  }},
		onSuccess: function() {
			Router.go('/');
			var currentDocId = this.docId;
			var currentDoc = Events.findOne(currentDocId);
			var url = currentDoc.slug;
			var rootUrl = Meteor.absoluteUrl();
			var date = currentDoc.date.toString();
			var description = currentDoc.eventdescription;
			var name = currentDoc.name;
			var location = currentDoc.location;
			// Meteor.call('scheduleNotification', currentDoc);
			sendNewEventEmail(url, rootUrl, date, description, name, location);
		}
	}
});

Template.registerHelper( 'eventLength', function() {
    return [
        {label: "1 hour", value: "1 hour"},
        {label: "2 hours", value: "2 hours"},
        {label: "3 hours", value: "3 hours"}
    ];
});

var sendNewEventEmail = function(url, rootUrl, date, description, name, location) {
	var dataContext = {
		message: "A new event has been proposed by another member and needs more participants.",
		url: url,
		rootUrl: rootUrl,
		date: date,
		name: name,
		location: location,
		description: description,
	};

	var html = Blaze.toHTMLWithData(Template.newEventEmail, dataContext);
	// console.log(html);

	var dateForSubjectLine = moment(date).format('ddd DD MMM');
	var subjectLine = 'Join us? ' + dateForSubjectLine;
	
	var options = {
			subject: subjectLine,
			html: html
	};

	Meteor.call('emailUsers', options);
};