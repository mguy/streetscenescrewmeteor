Template.userEmail.helpers({
  user: function() {
    return Meteor.user();
  },
  username: function () {
    return getUserName(Meteor.user());
  },
  users: function() {
    // underscore _.shuffle provides random sort
    return _.shuffle(Meteor.users.find().fetch());
  },
  requireEmail: function(user) {
    thisUser = Meteor.user();
    if (!getEmail(thisUser)) {
      return true;
    }
  },
  requireName: function(user) {
    thisUser = Meteor.user();
  if (!getUserName(thisUser)) {
    return true;
  }
}
});

Template.userEmail.events({
  'submit form': function(e){
    e.preventDefault();
    if(!Meteor.user())
      flashMessage(i18n.t('you_must_be_logged_in'), 'error');
    var $target=$(e.target);
    var user = Session.get('selectedUserId')? Meteor.users.findOne(Session.get('selectedUserId')) : Meteor.user();

    console.log(user);
    var update = {
      "profile.email": $target.find('[name=email]').val(),
      "profile.name": $target.find('[name=name]').val(),
    };
    
    Meteor.users.update(user._id, {
      $set: update
    }, function(error){
      if(error){
        error.reason, "error";
        $('.email-control').addClass('has-error');
        console.log(error);
      } else {
        Router.go('/');
      }
    });
  }

});