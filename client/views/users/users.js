getUserName = function(user){
  try{
    if (user.username)
      return user.username;
    if (user.profile.name)
        return user.profile.name;
    if (user && user.services && user.services.twitter && user.services.twitter.screenName)
      return user.services.twitter.screenName
  }
  catch (error){
    console.log(error);
    return null;
  }
};

getEmail = function(user){
  try {
    var userEmail = user.emails[0];
  }
  catch (e) {
    console.log(e);
  }
  if(user.profile && user.profile.email){
    return user.profile.email;
  } else if (userEmail) {
    return userEmail.address;
  } else {
    return null;
  }
};

userProfileCompleteChecks = [
  function(user) {
    return !!getEmail(user) && !!getUserName(user);
  }
];

userProfileComplete = function(user) {
  for (var i = 0; i < userProfileCompleteChecks.length; i++) {
    if (!userProfileCompleteChecks[i](user)) {
      return false;
    }
  }
  return true;
};